const webpack = require("webpack")
const merge = require('webpack-merge')
const HtmlWebpackPlugin = require("html-webpack-plugin")
const common = require('./webpack.common.js')

module.exports = merge(common, {
devServer: {
    port: 3000,
    open: true,
    hot: true,
    proxy: {
      "/api": "http://localhost:8080"
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/public/index.html"
      //   favicon: "./public/favicon.ico"
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ]
})
