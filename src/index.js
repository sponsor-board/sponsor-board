import React from 'react'
import {render} from 'react-dom'
// import { Helmet } from "react-helmet"
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Routing from './components/Routing/Routing' 
import Navigate from './components/Navigate/Navigate'
import './App.scss'



const App = (props) => (
    <div>
    
        <Navigate />
        <h1>Passport To Discovery</h1>
        <br />
        <h3>SPONSOR BOARD</h3>
        <Routing />
    </div>
)

render(
    <Router>
        <App />
    </Router>,
    document.getElementById('root')
)