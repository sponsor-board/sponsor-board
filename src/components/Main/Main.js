import React from 'react';
import Donator from '../Donator/Donator';
import Donation from '../Donation/Donation';
import FundWidget from '../FundWidget/FundWidget';
import ThankYou from '../ThankYou/ThankYou';
import './Main.scss';
const Main = props => (
  <div className="main-wrapper">
    <header>
      <h1>Welcome to Bella Danger's Page</h1>
    </header>

    <hr />
    <div className="fun-links">
      <h2>To donate, please visit:</h2>
      {/*<a href="http://bit.ly/PTDGoFundMe">Go Fund Me</a>*/}
    </div>
    <div className="embed">
      <FundWidget />
    </div>
    <div>
      <ul>
        <Donator />
      </ul>
    </div>
   
    <div className="main-container"><Donation /></div>
  </div>
);

export default Main;
