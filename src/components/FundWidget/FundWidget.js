import React from 'react'
import './FundWidget.scss'

const goFundMeWidget = `<iframe 
class='gfm-media-widget' 
image='1' 
coInfo='1' 
width='100%' 
height='100%' 
frameborder='0' id='help-izzy-compete-4-scholarships' />`


const FundWidget = (props) => (
    <div dangerouslySetInnerHTML={{ __html: goFundMeWidget}}>
    </div>
)

export default FundWidget