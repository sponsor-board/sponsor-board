import React from 'react'
import './ThankYou.scss'
const sponsors = [
    {"name": "ANONYMOUS DONATOR", amount: "$100"},
    {"name": "ANONYMOUS DONATOR", amount: "$100"},
    {"name": "ANONYMOUS DONATOR", amount: "$100"},
    {"name": "ANONYMOUS DONATOR", amount: "$100"},
    {"name": "Steve Gutting", amount: "$100"}
]
const ThankYou = (props) => (
    <div className="content">
  <div className="content__container">
    <p className="content__container__text">
      Thank you so much
    </p>
    
    <ul className="content__container__list">
    { sponsors.map((sponsor, i) =>
        <li key={i} className="content__container__list__item">{sponsor.name} !</li>
  )}
    </ul>
  </div>
</div>
)

export default ThankYou