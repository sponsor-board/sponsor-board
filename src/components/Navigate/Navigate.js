import React from "react";
import { Link, NavLink } from "react-router-dom";
import "./Navigate.scss";
const Navigate = props => (
  <div>
  <input type="checkbox" id="navcheck" role="button" title="menu" />
  <label htmlFor="navcheck" aria-hidden="true" title="menu">
    <span className="burger">
      <span className="bar">
        <span className="visuallyHidden"></span>
      </span>
    </span>
  </label>
  <nav id="menu">
  <NavLink to="/">Sponsor-Board</NavLink>
  <NavLink to="/Main">Main</NavLink>
  <NavLink to="/gallery">Photos</NavLink>
  </nav>
  </div>
)

export default Navigate
