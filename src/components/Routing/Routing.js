import React from 'react'
import {Route, Switch} from 'react-router-dom'
import PhotoGallery from '../PhotoGallery/PhotoGallery'
import Main from '../Main/Main'

 const Routing = (props) => (
    <div>
  
<Switch>
<Route exact path="/" component={Main} />
<Route path="/gallery" component={PhotoGallery} />
</Switch>

    </div>
)

export default Routing

