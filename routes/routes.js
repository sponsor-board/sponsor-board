const express = require('express')
const routeIndex = express.Router()

routeIndex.get('/', (req,res) => {
    res.send({ response: "I am working in routes"}).status(200)
})

module.exports = routeIndex