const express = require('express')
const http = require('http')
const socketIo = require('socket.io')
const axios = require('axios')

const port = process.env.PORT || 4001
const routeIndex = require('../routes/routes')

const app = express()
app.use(routeIndex)
const server = http.createServer(app)

const io = socketIo(server)

let interval
io.on("connection", socket => {
  console.log("New client connected")
  if (interval) {
    clearInterval(interval)
  }
  interval = setInterval(() => getApiAndEmit(socket), 10000)
  socket.on("disconnect", () => {
    console.log("Client disconnected")
  })
})

const getApiAndEmit = "TODO"
server.listen(port, () => console.log(`Listening on port ${port}`));